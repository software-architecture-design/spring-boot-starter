package net.architecture.springbootstarter.config;


import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
@SuppressWarnings("unused")
public class SwaggerConfig {

    /**
     * Produce a Swagger UI from all REST Endpoints.
     *
     * @return Swagger build.
     */
    @Bean
    public Docket produceApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("net.architecture.springbootstarter"))
                .paths(paths())
                .build();
    }

    /**
     * Produces a Swagger UI Description.
     *
     * @return API Build information.
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("REST Api Documentation")
                .description("This page lists all the REST Endpoints for this server.")
                .version("1.0-SNAPSHOT")
                .build();
    }

    /**
     * Only select apis that matches the given Predicates (except error).
     *
     * @return Return API Matches.
     */
    private Predicate<String> paths() {
        return Predicates.not(PathSelectors.regex("/error.*"));
    }
}