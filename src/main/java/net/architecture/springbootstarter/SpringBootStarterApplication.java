package net.architecture.springbootstarter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;


@Api(value = "/", description = "Main Server Entrypoint", produces = "application/json")
@RestController
@SpringBootApplication
public class SpringBootStarterApplication {

	private final static String FILE_NAME = "version.txt";

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStarterApplication.class, args);
	}

	@ApiOperation(value = "Gets the latest build information.", response = String.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Build Information", response = String.class),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/")
	public String getFileFromResources() {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream is = loader.getResourceAsStream(FILE_NAME);
		assert is != null;
		Scanner sc = new Scanner(is, StandardCharsets.UTF_8);
		StringBuilder sb = new StringBuilder();
		while (sc.hasNext()) {
			sb.append(sc.nextLine()).append('\n');
		}
		return sb.toString();
	}
}
